package com.mgmtp.sampleapp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mgmtp.sampleapp.model.Address;
import com.mgmtp.sampleapp.repository.AddressRepository;

/**
 * Implementation of {@link AddressService}
 *
 * @author tcaselli
 */
@Service
public class AddressServiceImpl implements AddressService
{

	@Autowired
	private AddressRepository addressRepository;

	@Override
	public void delete(Long id)
	{
		addressRepository.deleteById(id);
	}

	@Override
	public Optional<Address> find(Long id)
	{
		return addressRepository.findById(id);
	}

	@Override
	public List<Address> findAll()
	{
		return addressRepository.findAll();
	}

	@Override
	public Address save(Address address)
	{
		return addressRepository.save(address);
	}

}
