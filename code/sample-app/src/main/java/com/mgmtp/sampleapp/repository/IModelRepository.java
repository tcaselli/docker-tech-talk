package com.mgmtp.sampleapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * Super interface for model repositories
 *
 * @author tcaselli
 *
 * @param <T> the model type
 */
@NoRepositoryBean
public interface IModelRepository<T> extends JpaRepository<T, Long>, JpaSpecificationExecutor<T>, QuerydslPredicateExecutor<T>
{

}
