package com.mgmtp.sampleapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Home redirection to OpenAPI api documentation
 */
@Controller
public class ApiDocController
{

	@RequestMapping("/")
	public String index()
	{
		return "redirect:swagger-ui.html";
	}

}
