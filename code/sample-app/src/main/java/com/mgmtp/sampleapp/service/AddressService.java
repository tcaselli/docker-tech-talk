package com.mgmtp.sampleapp.service;

import java.util.List;
import java.util.Optional;

import com.mgmtp.sampleapp.model.Address;

public interface AddressService
{

	void delete(Long id);

	Optional<Address> find(Long id);

	List<Address> findAll();

	Address save(Address address);

}
