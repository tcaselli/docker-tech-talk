package com.mgmtp.sampleapp;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.mgmtp")
public class ApplicationStarter implements CommandLineRunner
{

	@Override
	public void run(final String... args) throws Exception
	{
		if (args.length > 0 && args[0].equals("exitcode"))
		{
			throw new ExitException();
		}
	}

	class ExitException extends RuntimeException implements ExitCodeGenerator
	{
		private static final long serialVersionUID = 1L;

		@Override
		public int getExitCode()
		{
			return 10;
		}
	}

	/**
	 * Entry point
	 *
	 * @param args
	 * @throws Exception
	 */
	public static void main(final String[] args) throws Exception
	{
		new SpringApplication(ApplicationStarter.class).run(args);
	}

}
