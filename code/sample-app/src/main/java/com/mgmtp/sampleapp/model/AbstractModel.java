package com.mgmtp.sampleapp.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * Abstract bean that all beans should extend
 *
 * @author tcaselli
 * @version $Revision$ Last modifier: $Author$ Last commit: $Date$
 */
@MappedSuperclass
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = AbstractModel.PUBLIC_ID_PROPERTY_NAME)
public abstract class AbstractModel
{

	/**
	 * The name of the ID property
	 */
	public static final String ID_PROPERTY_NAME = "id";
	/**
	 * The name of the public ID property
	 */
	public static final String PUBLIC_ID_PROPERTY_NAME = "publicId";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = ID_PROPERTY_NAME, unique = true, nullable = false, insertable = true, updatable = false)
	private Long id;

	// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
	// GETTERS / SETTERS
	// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

	/**
	 * @return the id
	 */
	public Long getId()
	{
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(final Long id)
	{
		this.id = id;
	}

	// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
	// EQUALS / HASHCODE METHODS
	// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

	@Override
	public boolean equals(final Object obj)
	{
		return obj instanceof AbstractModel
				&& (id == null && ((AbstractModel) obj).getId() == null && super.equals(obj) || id != null && id.equals(((AbstractModel) obj).getId()));
	}

	@Override
	public int hashCode()
	{
		return id == null ? super.hashCode() : id.hashCode();
	}
}
