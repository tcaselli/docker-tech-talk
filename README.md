# Overview

Goal of this project is to demonstrate a simple usage of these technologies

* virtualbox
* vagrant
* docker
* docker-compose
* spring-boot
* spring-data-jpa
* postgresql + pgadmin
* hibernate
* querydsl
* REST openapi (with spring config)
* Springfox swagger ui

This project can run on windows or linux or mac, with same configuration. (If you have windows 10, be careful, virtualbox cannot run if you have hyperv activated)

You will create a simple vagrant VM with docker, docker-compose, java & maven installed and then be able to launch a spring-boot app with docker-compose going through a step by step process.

# Installation process

1- Install virtual box
```
https://www.virtualbox.org/wiki/Downloads
```

2- Install vagrant
```
https://www.vagrantup.com/downloads.html
```

3- Launch vagrant
```sh
# Open a command line on root folder of the project (where you have Vagrantfile) and run 
vagrant up
```

4- Log in to vagrant
```sh
# Now you have a linux machine with docker, docker-compose, java & maven installed
# Let's connect to the machine
vagrant ssh
```

5- Switch user as root (better not to be annoyed with user rights :D)
```
sudo su -
```

6- Launch database & pgadmin & app with docker compose
```sh
# Go to app folder
cd /data/code/sample-app && mvn install

# Launch docker compose as daemon (environment variables are taken from .env file)
docker-compose up -d

# Wait for 30 seconds 

# Check all containers are started with (you should have 3 containers listed sample-app_sample-app_1, sample-app_pgadmin_1)
docker container ls
```

7- Browse pgadmin
```sh
# You have access to the app on port 8090 (80 in container, fowarded to 8090 in vagrant , forwarded to 8090 on your local machine)
# Open in browser :
http:localhost:8090
# See following chapter for pgadmin configuration
```

8- Browse app
```sh
# You have access to the app on port 8089 (8080 in container, fowarded to 8089 in vagrant , forwarded to 8089 on your local machine)
# Open in browser :
http:localhost:8089
```

9- Reload app after code modifications
```sh
# Rebuild the jar file, docker image and reload container
cd /data/code/sample-app && mvn install && docker-compose kill && docker-compose up -d --build
# An alias has been created for that in virtual machine, just run :
reloadapp
```

# Browse PGAdmin
You can access pgadmin at :

``` sh
url : http://localhost:8090
# Login and password are defined in .env file
login : admin
password : password
```

When you are logged in you need to add a new server to access the database.

1- Add new server :  
```
Right click on Servers in left menu and Create->Server...
```

2- Enter connection informations
```sh
# General tab
Name : db
# Connection tab
Host : db (this is the name of docker service, see docker-compose.yaml file)
Port : 5432 (see .env file, this is default port)
Maintenance database : dbname (see .env file)
Username : dbuser (see .env file)
Password : password (see .env file) 
``` 

# Some useful commands

```sh
# Build docker image
cd /data/code && docker build --tag=mgm/sample-app:1.0-SNAPSHOT --file=Dockerfile --build-arg JAR_NAME=sample-app-1.0-SNAPSHOT .

# Run docker image (create a container) (mgm is owner, sample-app is image name, 1.0-SNAPSHOT is version)
docker run mgm/sample-app:1.0-SNAPSHOT

# List containers
docker container ls

# Open console on container (containerId is retrievable from 'docker container ls')
docker exec -it <containerId> sh

# Open logs on container
docker logs <containerId>

# Remove all container
docker rm $(docker container ls -a -q) -f

# Remove all images
docker image rm $(docker image ls -a -q) -f

# Remove all volumes
docker volume rm $(docker volume ls -q)
```