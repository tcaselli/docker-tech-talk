#!/bin/bash
echo ">>>>>> [INIT] Switching current user to root..."
# Execute script as root for more convenience
sudo su -

DOCKER_COMPOSE_VERSION=1.23.2

echo ">>>>>> [INIT] Creating linux aliases..."
echo "alias ll='ls -al'" >> ~/.bashrc

# Update apt-get & install some packages
echo ">>>>>> [INIT] Installing docker compose..."
curl -L "https://github.com/docker/compose/releases/download/$DOCKER_COMPOSE_VERSION/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

NVM_VERSION=0.34.0
NODE_VERSION=11.12.0

# Install java & maven
apt-get update
apt-get install -y openjdk-8-jdk
echo "export JAVA_HOME=$(readlink -f /usr/bin/java | sed 's:jre/bin/java::')" >> ~/.bashrc && . ~/.bashrc
apt-get install -y maven

echo "alias installapp='cd /data/code/sample-app && mvn install && cd -'" >> ~/.bashrc
echo "alias dockerbuildapp='docker build --tag=mgm/sample-app:1.0-SNAPSHOT --file=Dockerfile --build-arg JAR_NAME=sample-app-1.0-SNAPSHOT .'" >> ~/.bashrc
echo "alias reloadapp='cd /data/code/sample-app && mvn install && docker-compose kill && docker-compose up -d --build'" >> ~/.bashrc

. ~/.bashrc

echo ">>>>>> [INIT] Finished Vagrant Setup..."