package com.mgmtp.sampleapp.repository;

import com.mgmtp.sampleapp.model.Address;

public interface AddressRepository extends IModelRepository<Address>
{

}
