package com.mgmtp.sampleapp.usertype;

/**
 * Shortcut class for Boolean hibernate type
 *
 * @author tcaselli
 * @version $Revision$ Last modifier: $Author$ Last commit: $Date$
 */
public class BooleanUserType
{

	/**
	 * Name of boolean type class
	 */
	public static final String NAME = "org.hibernate.type.BooleanType";

}
