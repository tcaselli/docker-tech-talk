package com.mgmtp.sampleapp.api.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mgmtp.sampleapp.api.AddressApi;
import com.mgmtp.sampleapp.data.AddressData;
import com.mgmtp.sampleapp.model.Address;
import com.mgmtp.sampleapp.service.AddressService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;

@Controller
@Api(value = "Address", description = "the Address API", tags = { "Address" })
@RequestMapping("/v1")
public class AddressApiController implements AddressApi
{

	@Autowired
	private AddressService addressService;

	// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
	// PUBLIC METHODS
	// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

	@Override
	public ResponseEntity<Void> deleteAddress(@ApiParam(value = "", required = true) @PathVariable("id") String id)
	{
		final Long privateId = Long.valueOf(id);
		addressService.delete(privateId);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	public ResponseEntity<AddressData> getAddress(@ApiParam(value = "", required = true) @PathVariable("id") String id)
	{
		final Long privateId = Long.valueOf(id);
		final Optional<Address> optionalAddress = addressService.find(privateId);
		return new ResponseEntity<>(convertModel(optionalAddress.get()), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<AddressData>> getAllAddresses()
	{
		return new ResponseEntity<>(addressService.findAll().stream().map(address -> convertModel(address)).collect(Collectors.toList()), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<AddressData> saveAddress(@ApiParam(value = "") @Valid @RequestBody AddressData addressData)
	{
		Address address = getOrCreateModel(addressData.getId());
		populateModel(addressData, address);
		address = addressService.save(address);
		return new ResponseEntity<>(convertModel(address), HttpStatus.OK);
	}

	// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
	// PRIVATE METHODS
	// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

	private AddressData convertModel(Address address)
	{
		AddressData data = null;
		if (address != null)
		{
			data = new AddressData();
			data.setId(address.getId() == null ? null : address.getId().toString());
			data.setName(address.getName());
			data.setStreet(address.getStreet());
			data.setCity(address.getCity());
			data.setZipCode(address.getZipCode());
			data.setCountryCode(address.getCountryCode());
		}
		return data;
	}

	private void populateModel(AddressData data, Address address)
	{
		address.setName(data.getName());
		address.setStreet(data.getStreet());
		address.setCity(data.getCity());
		address.setZipCode(data.getZipCode());
		address.setCountryCode(data.getCountryCode());
	}

	private Address getOrCreateModel(String id)
	{
		return id == null ? new Address() : addressService.find(Long.valueOf(id)).get();
	}

}
